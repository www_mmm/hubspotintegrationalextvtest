﻿using HubSpotIntegrationAlexTvTest.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;

namespace HubSpotIntegrationAlexTvTest.Export
{
    public class OpenXmlExport
    {
        private readonly string _tableName;
        private readonly string _path;
        private Dictionary<int, int> _columnsWidth = new Dictionary<int, int>();

        public OpenXmlExport()
        {
            _tableName = "Export";

            var fileName =
                $"Export_{DateTime.Now.ToString("ddMMyyyy_HHmmss")}.xlsx";

            _path = System.IO.Path.Combine(
                System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), fileName);
        }

        public void ExportAndOpen(List<Contact> contacts)
        {
            try
            {
                using (SpreadsheetDocument spreadsheet =
                    SpreadsheetDocument.Create(_path,
                        SpreadsheetDocumentType.Workbook))
                {
                    WorkbookPart workbookPart = spreadsheet.AddWorkbookPart();
                    workbookPart.Workbook = new Workbook();

                    WorksheetPart worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
                    SheetData sheetData = new SheetData();
                    Sheets sheets = new Sheets();
                    Sheet sheet = new Sheet
                    {
                        Id = spreadsheet.WorkbookPart.GetIdOfPart(worksheetPart),
                        SheetId = 1,
                        Name = _tableName
                    };
                    sheets.Append(sheet);

                    Row row = new Row { RowIndex = (uint)1 };
                    row.AppendChild(AddCell(0, row.RowIndex, "Vid"));
                    row.AppendChild(AddCell(1, row.RowIndex, "First Name"));
                    row.AppendChild(AddCell(2, row.RowIndex, "Last Name"));
                    row.AppendChild(AddCell(3, row.RowIndex, "LifecycleStage"));
                    row.AppendChild(AddCell(4, row.RowIndex, "Associated Company Id"));
                    row.AppendChild(AddCell(5, row.RowIndex, "Associated Company Name"));
                    row.AppendChild(AddCell(6, row.RowIndex, "Associated Company WebSite"));
                    row.AppendChild(AddCell(7, row.RowIndex, "Associated Company City"));
                    row.AppendChild(AddCell(8, row.RowIndex, "Associated Company State"));
                    row.AppendChild(AddCell(9, row.RowIndex, "Associated Company Zip"));
                    row.AppendChild(AddCell(10, row.RowIndex, "Associated Company Phone"));
                    sheetData.Append(row);

                    for (int i = 1; i <= contacts.Count; i++)
                    {
                        row = new Row { RowIndex = (uint)i + 1 };

                        var contact = contacts[i - 1];

                        row.AppendChild(AddCell(0, row.RowIndex, contact.Vid.ToString()));
                        row.AppendChild(AddCell(1, row.RowIndex, contact.FirstName));
                        row.AppendChild(AddCell(2, row.RowIndex, contact.LastName));
                        row.AppendChild(AddCell(3, row.RowIndex, contact.LifecycleStage));
                        row.AppendChild(AddCell(4, row.RowIndex, contact.AssociatedCompany?.CompanyId?.ToString()));
                        row.AppendChild(AddCell(5, row.RowIndex, contact.AssociatedCompany?.Name));
                        row.AppendChild(AddCell(6, row.RowIndex, contact.AssociatedCompany?.WebSite));
                        row.AppendChild(AddCell(7, row.RowIndex, contact.AssociatedCompany?.City));
                        row.AppendChild(AddCell(8, row.RowIndex, contact.AssociatedCompany?.State));
                        row.AppendChild(AddCell(9, row.RowIndex, contact.AssociatedCompany?.Zip));
                        row.AppendChild(AddCell(10, row.RowIndex, contact.AssociatedCompany?.Phone));

                        sheetData.Append(row);
                    }

                    worksheetPart.Worksheet = new Worksheet(sheetData);

                    var listColumns = new Columns();
                    for (UInt32 columnIndex = 0; columnIndex < _columnsWidth.Count; columnIndex++)
                    {
                        listColumns.Append(new Column()
                        {
                            Min = (columnIndex + 1),
                            Max = columnIndex + 1,
                            Width = _columnsWidth[(int)columnIndex],
                            CustomWidth = true
                        });
                    }
                    worksheetPart.Worksheet.InsertAt(listColumns, 0);

                    spreadsheet.WorkbookPart.Workbook.AppendChild<Sheets>(sheets);
                    workbookPart.Workbook.Save();

                    spreadsheet.Close();
                }

                Process.Start(_path);
            }
            catch (Exception e)
            {
                Console.WriteLine("Export error:" + e);
                throw;
            }
        }

        private Cell AddCell(int columnIndex, UInt32Value rowIndex, string textValue)
        {
            textValue = textValue ?? "";
            Cell cell = new Cell
            {
                CellReference = CreateCellReference(columnIndex + 1) + rowIndex,
                DataType = CellValues.InlineString,
            };

            InlineString inlineString = new InlineString();
            Text text = new Text
            {
                Text = textValue
            };
            inlineString.Text = text;
            cell.AppendChild(inlineString);

            //
            var widthByText = textValue.Length + 2;
            if (_columnsWidth.ContainsKey(columnIndex))
            {
                if (_columnsWidth[columnIndex] < widthByText)
                {
                    _columnsWidth[columnIndex] = widthByText;
                }
            }
            else
            {
                _columnsWidth.Add(columnIndex, widthByText);
            }

            return cell;
        }

        private string CreateCellReference(int column)
        {
            string result = string.Empty;

            char firstRef = 'A';
            uint firstIndex = firstRef;
            int mod;

            while (column > 0)
            {
                mod = (column - 1) % 26;
                result += (char)(firstIndex + mod);
                column = (column - mod) / 26;
            }

            return result;
        }
    }
}