﻿namespace HubSpotIntegrationAlexTvTest.Models
{
    public class Company
    {
        public long? CompanyId { get; set; }
        public string Name { get; set; }
        public string WebSite { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Phone { get; set; }
    }
}