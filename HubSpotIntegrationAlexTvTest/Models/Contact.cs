﻿using Newtonsoft.Json;

namespace HubSpotIntegrationAlexTvTest.Models
{
    public class Contact
    {
        public int Vid { get; set; }

        [JsonProperty(PropertyName = "firstname")]
        public string FirstName { get; set; }

        [JsonProperty(PropertyName = "lastname")]
        public string LastName { get; set; }
        
        [JsonProperty(PropertyName = "lifecyclestage")]
        public string LifecycleStage { get; set; }

        public Company AssociatedCompany { get; set; }
    }
}