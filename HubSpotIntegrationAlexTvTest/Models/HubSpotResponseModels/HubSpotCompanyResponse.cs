﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace HubSpotIntegrationAlexTvTest.Models.HubSpotResponseModels
{
    public class HubSpotCompanyResponse
    {
        [JsonProperty(PropertyName = "companyId")]
        public long CompanyId { get; set; }
        
        [JsonProperty(PropertyName = "properties")]
        public Dictionary<string, HubSpotValue> Properties { get; set; }


        public string Name => PropertyValueHelpers.GetStringValue(Properties, "name");
        public string WebSite => PropertyValueHelpers.GetStringValue(Properties, "website");
        public string City => PropertyValueHelpers.GetStringValue(Properties, "city");
        public string State => PropertyValueHelpers.GetStringValue(Properties, "state");
        public string Zip => PropertyValueHelpers.GetStringValue(Properties, "zip");
        public string Phone => PropertyValueHelpers.GetStringValue(Properties, "phone");
    }
}