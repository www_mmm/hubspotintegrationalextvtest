﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace HubSpotIntegrationAlexTvTest.Models.HubSpotResponseModels
{
    public class HubSpotContact
    {
        [JsonProperty(PropertyName = "vid")]
        public int Vid { get; set; }

        [JsonProperty(PropertyName = "properties")]
        public Dictionary<string, HubSpotValue> Properties { get; set; }

        public string FirstName => PropertyValueHelpers.GetStringValue(Properties,"firstname");
        public string LastName => PropertyValueHelpers.GetStringValue(Properties, "lastname");
        public string LifecycleStage => PropertyValueHelpers.GetStringValue(Properties, "lifecyclestage");
        public long? AssociatedCompanyId => PropertyValueHelpers.GetLongValue(Properties, "associatedcompanyid");
        public DateTime? LastUpdated => PropertyValueHelpers.GetDateTimeValue(Properties, "lastmodifieddate");
    }
}