﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace HubSpotIntegrationAlexTvTest.Models.HubSpotResponseModels
{
    public class HubSpotRecentlyUpdatedResponse
    {
        [JsonProperty(PropertyName = "has-more")]
        public bool HasMore { get; set; }

        [JsonProperty(PropertyName = "vid-offset")]
        public long VidOffset { get; set; }

        [JsonProperty(PropertyName = "time-offset")]
        public long TimeOffset { get; set; }

        public DateTime? TimeOffsetDateTime
        {
            get
            {
                if (TimeOffset > 0)
                {
                    return PropertyValueHelpers.LongToDateTime(TimeOffset);
                }
                return null;
            }
        }

        [JsonProperty(PropertyName = "contacts")]
        public List<HubSpotContact> Contacts { get; set; }

        public List<Contact> GetContacts(DateTime modifiedOnOrAfter)
        {
            return Contacts.Where(w => w.LastUpdated >= modifiedOnOrAfter).Select(s => new Contact
            {
                Vid = s.Vid,
                LifecycleStage = s.LifecycleStage,
                FirstName = s.FirstName,
                LastName = s.LastName,
                AssociatedCompany = new Company
                {
                    CompanyId = s.AssociatedCompanyId
                }
            }).ToList();
        }
    }
}