﻿using Newtonsoft.Json;

namespace HubSpotIntegrationAlexTvTest.Models.HubSpotResponseModels
{
    public class HubSpotValue
    {
        [JsonProperty(PropertyName = "value")]
        public string Value { get; set; }
    }
}