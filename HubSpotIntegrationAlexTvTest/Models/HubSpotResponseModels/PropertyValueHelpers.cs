﻿using System;
using System.Collections.Generic;

namespace HubSpotIntegrationAlexTvTest.Models.HubSpotResponseModels
{
    public class PropertyValueHelpers
    {
        public static string GetStringValue(Dictionary<string, HubSpotValue> properties, string fieldName)
        {
            if (properties.ContainsKey(fieldName))
            {
                return properties[fieldName].Value;
            }

            return "";
        }

        public static long? GetLongValue(Dictionary<string, HubSpotValue> properties, string fieldName)
        {
            if (properties.ContainsKey(fieldName) && long.TryParse(properties[fieldName].Value, out var longValue))
            {
                return longValue;
            }

            return null;
        }
        public static DateTime? GetDateTimeValue(Dictionary<string, HubSpotValue> properties, string fieldName)
        {
            if (properties.ContainsKey(fieldName) && long.TryParse(properties[fieldName].Value, out var longValue))
            {
                return LongToDateTime(longValue);
            }

            return null;
        }

        public static DateTime LongToDateTime(long value)
        {
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            var timeSpan = TimeSpan.FromMilliseconds(value);
            return epoch.Add(timeSpan).ToLocalTime();
        }
    }

}