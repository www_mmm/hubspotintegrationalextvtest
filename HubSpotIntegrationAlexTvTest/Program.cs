﻿using System;
using HubSpotIntegrationAlexTvTest.Wrappers;
using System.Configuration;
using System.Globalization;
using HubSpotIntegrationAlexTvTest.Export;

namespace HubSpotIntegrationAlexTvTest
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("Enter a date when or after contacts were modified (dd.MM.yyyy):");
            if (DateTime.TryParseExact(Console.ReadLine(), "dd'.'MM'.'yyyy",
                CultureInfo.CurrentCulture, DateTimeStyles.None, out var userDateTime))
            {
                Console.WriteLine("The date is correct!");
                try
                {
                    var modifiedOnOrAfterUtc = userDateTime;//new DateTime(2019, 1, 24);
                    var hubSpotWrapper = new HubSpotWrapper(ConfigurationManager.AppSettings["HapiKey"]);

                    //METHOD A
                    var contacts = hubSpotWrapper.GetContacts(modifiedOnOrAfterUtc);

                    Console.WriteLine($"{contacts.Count} contacts were found.");

                    //METHOD B
                    new OpenXmlExport().ExportAndOpen(contacts);

                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Something went wrong:{ex.Message}");
                }
            }
            else
            {
                Console.WriteLine("You have entered an incorrect value.");
            }
            Console.ReadLine();
        }
    }
}