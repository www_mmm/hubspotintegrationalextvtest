﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using HubSpotIntegrationAlexTvTest.Models;
using HubSpotIntegrationAlexTvTest.Models.HubSpotResponseModels;
using Newtonsoft.Json;

namespace HubSpotIntegrationAlexTvTest.Wrappers
{
    public class HubSpotWrapper
    {
        private const string HubspotRerecentlyUpdatedUrl =
            "https://api.hubapi.com/contacts/v1/lists/recently_updated/contacts/recent";

        private const string HubspotGetCompanyUrl = "https://api.hubapi.com/companies/v2/companies/";

        private const int HubspotPageSize = 10;

        private readonly string _hApiKey;
        public HubSpotWrapper(string hApiKey)
        {
            _hApiKey = hApiKey;
        }
        
        public List<Contact> GetContacts(DateTime modifiedOnOrAfter)
        {
            List<Contact> contacts = new List<Contact>();
            
            
            var hubspotResponseModel = HubSpotRecentlyUpdatedRequest();
            contacts.AddRange(hubspotResponseModel.GetContacts(modifiedOnOrAfter));

            while (hubspotResponseModel.HasMore && hubspotResponseModel.TimeOffsetDateTime.HasValue &&
                   hubspotResponseModel.TimeOffsetDateTime >= modifiedOnOrAfter)
            {
                hubspotResponseModel = HubSpotRecentlyUpdatedRequest(hubspotResponseModel.VidOffset, hubspotResponseModel.TimeOffset);
                contacts.AddRange(hubspotResponseModel.GetContacts(modifiedOnOrAfter));
            }

            PopulateAssociatedCompanies(ref contacts);

            return contacts;
        }

        private void PopulateAssociatedCompanies(ref List<Contact> contacts)
        {
            var companies = GetCompanies(
                contacts.Where(w => w.AssociatedCompany.CompanyId.HasValue)
                        .Select(s => s.AssociatedCompany.CompanyId??0)
                        .Distinct().ToList()
                );
            contacts.ForEach(ac=>ac.AssociatedCompany = companies.FirstOrDefault(f=>f.CompanyId==ac.AssociatedCompany.CompanyId));
        }

        

        public List<Company> GetCompanies(List<long> companyIds)
        {
            List<Company> companies = new List<Company>();
            foreach (var id in companyIds)
            {
                var companyResponse = HubSpotCompanyByIdRequest(id);
                companies.Add(new Company
                {
                    CompanyId = companyResponse.CompanyId,
                    City = companyResponse.City,
                    Name = companyResponse.Name,
                    Phone = companyResponse.Phone,
                    State = companyResponse.State,
                    WebSite = companyResponse.WebSite,
                    Zip = companyResponse.Zip
                });
            }

            return companies;
        }


        private HubSpotRecentlyUpdatedResponse HubSpotRecentlyUpdatedRequest(long? vidOffset = null, long? timeOffset = null)
        {
            try
            {
                string url =
                    $"{HubspotRerecentlyUpdatedUrl}?" +
                    $"hapikey={_hApiKey}" +
                    $"&count={HubspotPageSize}" +
                    $"&vidOffset={vidOffset}" +
                    $"&timeOffset={timeOffset}" +
                    $"&property=lifecyclestage" +
                    $"&property=firstname" +
                    $"&property=lastname" +
                    $"&property=lastmodifieddate" +
                    $"&property=associatedcompanyid";

                var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "GET";

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();

                using (var streamReader = new StreamReader(httpResponse.GetResponseStream() ?? throw new Exception("Response is empty!")))
                {
                    var response = streamReader.ReadToEnd();
                    return JsonConvert.DeserializeObject<HubSpotRecentlyUpdatedResponse>(response);
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine($"Error{exception.Message}");
            }

            return null;
        }


        private HubSpotCompanyResponse HubSpotCompanyByIdRequest(long id)
        {
            try
            {
                string url = $"{HubspotGetCompanyUrl}{id}?hapikey={_hApiKey}";
                    
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "GET";

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();

                using (var streamReader = new StreamReader(httpResponse.GetResponseStream() ?? throw new Exception("Response is empty!")))
                {
                    var response = streamReader.ReadToEnd();
                    return JsonConvert.DeserializeObject<HubSpotCompanyResponse>(response);
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine($"Error:{exception.Message}");
            }

            return null;
        }
    }
}
